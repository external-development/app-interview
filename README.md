## Project Specs
##### Last Updated on January 10, 2019


#### Introduction
Welcome to the development portion for Mobile Application Developer. This project is to test your development skills and Design skills in order for us to grasp your experience in both areas. We are looking for a sharp attention to detail in both areas as they are both critical to producing a professional grade application.

#### Project Overview
Now that you have a general understanding of our business, this (unofficial) prototype will act as a feature addition for the sake of this project.

The goal of this project is to create a quick checkout system for drivers to request cars. You are to assume the user has already signed up (or logged in) and they are now on the primary page of the application. Here, they are presented with cards, which display information about cars within a certain radius of their device location.

On the left screen, you will see the basic details that are required, each labeled in detail in the specs below. The right screen is the detail screen. This screen can be displayed based on your liking (either navigation, presented on top, or gesture). The detail screen will show further details of the selected car.

The left screen should navigate through different cards using the Cocoa-pod, Poi, which can be found in the [Minimum Requirements](https://gitlab.com/external-development/app-interview#minimum-requirementsx) section below. This will allow for a Tinder-esque style to swipe between different available offers.

#### Submission
The final project submission should be a push to this repository containing the following elements:
- XCode Workspace
- Pod related files
  - Podfile
  - Podfile.lock
  - Pods/
- Class Files
- Additional Documentation
- Any other files required to allow an outside developer quick access to clone the repository and build the project on their device.

You may create any branches you need within Git, as you progress, however, only the master branch will be verified.

Please note that while we encourage Swift, as Swift is the primary language used on iOS at HyreCar, we will accept code written in Objective-C.

##### Minimum Requirements
![Main Screens](imgs/Screens.png)
##### Left Screen
The left screen has 9 main elements:
1. Navigation bar with title
2. Car Image
3. Car Year
4. Car Make and Model
5. Selected Date Range
6. Price Per Day
7. Skip button (button A)
8. Details Button (button B)
9. Request Button (button C)

Most of these elements can be assigned from a Car object and displayed as the user navigates through the application. The data presented could be managed through an array of Cars, managed through the Poi Data Source.

The actions on this page should be:
- For Button A:
  - Show an alert that this car was skipped
  - Once the alert is dismissed, show the next car
- For Button B:
  - Show the Car Details Page
  - This is where you can show off your design and code skills for the transition (optional)
- For Button C:
  - Show an alert that this car was added to be requested
  - Once the alert is dismissed, show the next car.

Poi is a Cocoapod that manages the swipe to accept or skip action. The swipe animation is already built, however, you may customize this if you desire. This cocoapod can be integrated by utilizing this [link](https://cocoapods.org/pods/Poi) (recommended use), or by following the instructions on the GitHub Repo [here](https://github.com/HideakiTouhara/Poi).

##### Right Screen
The right screen, otherwise known as the "Car Detail Page", contains about 9 Elements:
1. Navigation Bar with Title
2. Car Image View
3. Car Year
4. Car Make and Model
5. Selected Date Range
6. Price Per Day
7. Back button (button D)
8. Details Header Label
9. Details Description Label

Note that you may add more elements to this page if desired, however, we are looking for at least the items displayed above.

There is a single action on this page:
- On button tap (and gesture if desired, button must still be present), the view should return to the previous screen with the selected car still shown.

#### Other Details
Note that we are primarily looking for the features shown above, however, we welcome additional customization anywhere you seem fit. This small project should take between 7 - 10 hours, but must be submitted (with documentation) no later than Friday, January 18, 2019.

If any schedule conflicts arise, or if you have any questions, please feel free to reach out to the Lead iOS Developer, Tizzle, [here](mailto:tizzle@hyrecar.com).

#### Conclusion
This is your time to shine! As noted, we are looking for the functionality detailed above, but we welcome creativity! This code is yours to keep and will not be used in production code. Note, however, you should keep a local copy as access to GitLab will be restricted.

__Good Luck!__
